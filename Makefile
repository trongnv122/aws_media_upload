
CODE_NAME ?= aws_media
STACK_NAME ?= ${CODE_NAME}-${ENV}

clean:
	rm -rf build


build:
	mkdir ./build
	rm -rf src/node_modules
	yarn install --cwd ./src
	unzip layer.zip -d ./build

build/output.yaml: template.yaml build
	mkdir -p build
	aws configure set aws_access_key_id $(access_key_id)
	aws configure set aws_secret_access_key $(secret_access_key)
	aws configure set default.region $(region)
	aws cloudformation package --template $< --s3-bucket $(DEPLOYMENT_BUCKET) \
		--output-template-file $@

	@if [ ${ENV} = "prod" ]; then\
		echo "stack name $(STACK_NAME) replace with prod ENVIRONMENT";\
		sed -i'' -e "s|"-DEPLOY_ENV"|""|g" build/output.yaml;\
	else\
		echo "stack name $(STACK_NAME) replace with ${ENV} ENVIRONMENT";\
		sed -i'' -e "s|"-DEPLOY_ENV"|-${ENV}|g" build/output.yaml;\
	fi

	sed -i'' -e "s|"CODE_NAME"|${CODE_NAME}|g" build/output.yaml;\

deploy: build/output.yaml
	aws cloudformation deploy --template $< --stack-name $(STACK_NAME) \
		--parameter-overrides  AppCode=$(CODE_NAME) \
		--capabilities CAPABILITY_IAM CAPABILITY_AUTO_EXPAND
	aws cloudformation describe-stacks --stack-name $(STACK_NAME) --query Stacks[].Outputs --output table
