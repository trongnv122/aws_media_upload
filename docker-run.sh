#!/bin/bash

echo "$(cat logo.txt)"

source .env
echo "deploy from bucket $DEPLOYMENT_BUCKET"
echo "access_key_id ...$access_key_id" | cut -c1-30
echo "secret_access_key ...$secret_access_key" | cut -c1-30
echo "region $region"
echo "CODE_NAME $CODE_NAME"
echo "ENV $ENV"

docker-compose -f docker/docker-compose.yml up
