const s3Util                = require('./s3-util');
const childProcessPromise   = require('./child-process-promise');
const path                  = require('path');
const os                    = require('os');
const fs                    = require('fs');

const OUTPUT_BUCKET         = process.env.OUTPUT_BUCKET;
const REGION                = process.env.REGION;

const EXTENSION             = ".mp3";
const MIME_TYPE             = "audio/mpeg";
const baseUploadPath        = 'audio/';

const messageType           = "AudioUploadCompleted";

const sendViaHttp           = process.env.SEND_VIA_HTTP;
const sendViaSNS            = process.env.SEND_VIA_SNS;


exports.handler = async function (eventObject, context, callback) {
    try {
        const eventRecord       =   eventObject.Records && eventObject.Records[0];
        const inputBucket       =   eventRecord.s3.bucket.name;
        const key               =   eventRecord.s3.object.key;
        const id                =   context.awsRequestId;
        const workdir           =   os.tmpdir();
        const inputFile         =   path.join(workdir, id + path.extname(key));
        const outputFile        =   path.join(workdir, id + '_out' + EXTENSION);

        console.log("convert audio inputFile " + inputFile);
        console.log("convert audio outputFile " + outputFile);

        let headObject = await s3Util.getHeadObject(OUTPUT_BUCKET, key);
        let metadata = headObject['Metadata'] || {};

        console.log("convert audio with metadata " + JSON.stringify(metadata))

        if (!metadata.guid) {
            console.log("guid empty, return convert audio");
            return;
        }

        const idx = key.lastIndexOf('/');
        const extendPath = key.substring(baseUploadPath.length, idx);

        console.log('converting', inputBucket, key, 'using', inputFile);

        await s3Util.downloadFileFromS3(inputBucket, key, inputFile);

        await childProcessPromise.spawn(
            '/opt/bin/ffmpeg',
            ["-y", "-loglevel", "info", '-i', inputFile,
                "-codec:a", "libmp3lame",
                "-b:a", "128k",
                outputFile],
            {env: process.env, cwd: workdir}
        );

        let resultKey = `${baseUploadPath}${extendPath}/${metadata.guid}-128${EXTENSION}`
            .replace(/\b(?!:)\/{2,}/g, '/');

        await s3Util.uploadFileToS3(OUTPUT_BUCKET, resultKey, outputFile, MIME_TYPE);

        const variantSize = {"audio": resultKey};

        let message = {guid: metadata.guid, bucket: OUTPUT_BUCKET, region: REGION, type: messageType, metadata: {variantSize}};

        if (sendViaHttp === 'true' && metadata.callback_url) {
            s3Util.callbackConvert(metadata.callback_url, message);
        }
        if (sendViaSNS === 'true') {
            await s3Util.sendToSNS(message);
        }

        s3Util.removeFile(inputFile);
        s3Util.removeFile(outputFile);

        if (!fs.existsSync(inputFile)) {
            console.log("Input file removed");
        }
        if (!fs.existsSync(outputFile)) {
            console.log("Output file removed");
        }

    } catch (err) {
        console.log(`convert audio error: ${err}`);
    }

    console.log("Convert audio done");
    callback();
};
