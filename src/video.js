const s3Util = require('./s3-util'),
    childProcessPromise = require('./child-process-promise'),
    path = require('path'),
    os = require('os'),

    OUTPUT_BUCKET = process.env.OUTPUT_BUCKET,
    REGION = process.env.REGION,

    MIME_TYPE = "video/mp4",
    EXTENSION = ".mp4",
    THUMB_MIME_TYPE = "image/jpeg",
    THUMB_EXTENSION = "_thumb.jpg",

    baseUploadPath = 'video/',

    messageType = "VideoUploadCompleted";

const fs                    = require('fs');

const sendViaHttp           = process.env.SEND_VIA_HTTP;
const sendViaSNS            = process.env.SEND_VIA_SNS;

exports.handler = async function (eventObject, context, callback) {
    try {
        const eventRecord = eventObject.Records && eventObject.Records[0],
            inputBucket = eventRecord.s3.bucket.name,
            key = eventRecord.s3.object.key,
            id = context.awsRequestId,
            workdir = os.tmpdir(),
            inputFile = path.join(workdir, id + path.extname(key)),
            outputFile = path.join(workdir, id + '_out' + EXTENSION)
        ;

        console.log("convert video inputFile " + inputFile);
        console.log("convert video outputFile " + outputFile);

        let headObject = await s3Util.getHeadObject(OUTPUT_BUCKET, key);
        let metadata = headObject['Metadata'] || {};

        console.log("convert video with metadata " + JSON.stringify(metadata))

        if (!metadata.guid) {
            console.log("guid empty, return convert video");
            return;
        }

        const idx = key.lastIndexOf('/');
        const extendPath = key.substring(baseUploadPath.length, idx);

        let resultKey = `${baseUploadPath}${extendPath}/${metadata.guid}-aac${EXTENSION}`
            .replace(/\b(?!:)\/{2,}/g, '/');

        const thumbFile = path.join(workdir, id + THUMB_EXTENSION);
        let thumbKey = `${baseUploadPath}${extendPath}/${metadata.guid}${THUMB_EXTENSION}`
            .replace(/\b(?!:)\/{2,}/g, '/');

        console.log('converting ', inputBucket, key, 'using ', inputFile);
        console.log('thumbFile ', thumbFile, 'thumbKey ', thumbKey);

        await s3Util.downloadFileFromS3(inputBucket, key, inputFile);

        // Convert to mp4 standard
        await childProcessPromise.spawn(
            '/opt/bin/ffmpeg',
            ["-loglevel", "info", '-i', inputFile,
                "-preset", "superfast",
                "-movflags", "+faststart",
                "-tune", "fastdecode",
                "-crf", "25",
                "-bufsize", "2M",
                "-c:a", "aac",
                outputFile],
            {env: process.env, cwd: workdir}
        );

        // Extract thumbnail
        await childProcessPromise.spawn(
            '/opt/bin/ffmpeg',
            ["-loglevel", "info",
                '-i', inputFile,
                "-ss", "00:00:01.000",
                "-vframes", "1",
                thumbFile
            ],
            {env: process.env, cwd: workdir}
        );

        await s3Util.uploadFileToS3(OUTPUT_BUCKET, thumbKey, thumbFile, THUMB_MIME_TYPE);

        await s3Util.uploadFileToS3(OUTPUT_BUCKET, resultKey, outputFile, MIME_TYPE);

        const variantSize = {video: resultKey, thumbnail: thumbKey};

        let message = {guid: metadata.guid, bucket: OUTPUT_BUCKET, region: REGION, type: messageType, metadata: {variantSize}};

        if (sendViaHttp === 'true' && metadata.callback_url) {
            s3Util.callbackConvert(metadata.callback_url, message);
        }
        if (sendViaSNS === 'true') {
            await s3Util.sendToSNS(message);
        }

        s3Util.removeFile(inputFile);
        s3Util.removeFile(outputFile);

        if (!fs.existsSync(inputFile)) {
            console.log("Input file removed");
        }
        if (!fs.existsSync(outputFile)) {
            console.log("Output file removed");
        }
    } catch (err) {
        console.log(`Error convert video file: ${err}`);
    }

    console.log("Convert video done");
    callback();
};
