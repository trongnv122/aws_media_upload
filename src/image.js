const sharp = require('sharp');
const aws = require('aws-sdk');
const s3 = new aws.S3();
const s3Util = require('./s3-util');
const jo = require('jpeg-autorotate');

const REGION = process.env.REGION;
const Bucket = process.env.OUTPUT_BUCKET;

const baseUploadPath = 'image/';

const messageType = "ImageUploadCompleted";

const transforms = [
    {name: 's', width: 480, height: 320},
    {name: 'm', width: 720, height: 480},
    {name: 'l', width: 1080, height: 720},
];

const sendViaHttp = process.env.SEND_VIA_HTTP;
const sendViaSNS = process.env.SEND_VIA_SNS;

// Rotate an image given a buffer
const autorotateImage = async function (data) {
    return new Promise(function (resolve, reject) {
        jo.rotate(data, {}, function (error, buffer, orientation) {
            if (error) {
                console.log('An error occurred when rotating the file: ' + error.message);
                resolve(null);
            } else {
                console.log('Orientation was: ' + orientation);
                resolve(buffer);
            }
        });
    });
};

exports.handler = async (event, context, callback) => {
    const key = event.Records[0].s3.object.key;
    const ext = key.match(/(?:\.([^.]+))?$/)[1].toLowerCase();
    const format = ext === "png" ? "png" : "jpeg";

    const idx = key.lastIndexOf('/');
    const extendPath = key.substring(baseUploadPath.length, idx);

    try {
        let headObject = await s3Util.getHeadObject(Bucket, key);
        let metadata = headObject['Metadata'] || {};

        console.log("callbackConvert with metadata " + JSON.stringify(metadata))

        if (!metadata.guid) {
            console.log("guid empty, return callbackConvert");
            return;
        }

        const data = await s3Util.getObject(Bucket, key);

        const image = await autorotateImage(data.Body);

        let variantSize = {};

        if (ext === "gif") {
            for (const t of transforms) {
                variantSize[t.name] = key;
            }
        } else {
            for (const t of transforms) {
                let sizeKey = `${baseUploadPath}${extendPath}/${metadata.guid}_${t.name}.${ext}`
                    .replace(/\b(?!:)\/{2,}/g, '/');

                await sharp(image ? image : data.Body)
                    .resize({width: t.width})
                    .toFormat(format)
                    .toBuffer()
                    .then(buffer => s3.putObject({
                            Body: buffer,
                            Bucket: Bucket,
                            ContentType: 'image/' + format,
                            Key: sizeKey,
                            ACL: 'public-read',
                        }).promise()
                    );

                variantSize[t.name] = sizeKey;
            }
        }

        let message = {guid: metadata.guid, bucket: Bucket, region: REGION, type: messageType, metadata: {variantSize}};

        if (sendViaHttp === 'true' && metadata.callback_url) {
            s3Util.callbackConvert(metadata.callback_url, message);
        }
        if (sendViaSNS === 'true') {
            await s3Util.sendToSNS(message);
        }

    } catch (err) {
        console.log(`Error resizing files: ${err}`);
    }

    console.log("Convert image done");
    callback();
};