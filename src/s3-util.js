const aws           = require('aws-sdk');
const REGION        = process.env.REGION;

const fs            = require('fs');
const s3            = new aws.S3();
const https         = require('https');

const TOPIC_ARN     = process.env.TOPIC_ARN,

    downloadFileFromS3 = function (bucket, fileKey, filePath) {
        'use strict';
        console.log('downloading', bucket, fileKey, filePath);
        return new Promise(function (resolve, reject) {
            const file = fs.createWriteStream(filePath),
                stream = s3.getObject({
                    Bucket: bucket,
                    Key: fileKey
                }).createReadStream();
            stream.on('error', reject);
            file.on('error', reject);
            file.on('finish', function () {
                console.log('downloaded', bucket, fileKey);
                resolve(filePath);
            });
            stream.pipe(file);
        });
    },
    uploadFileToS3 = function (bucket, fileKey, filePath, contentType) {
        'use strict';
        console.log('uploading', bucket, fileKey, filePath);
        return s3.upload({
            Bucket: bucket,
            Key: fileKey,
            Body: fs.createReadStream(filePath),
            ACL: 'public-read',
            ContentType: contentType
        }).promise();
    },
    sendToSNS = async function (message) {
        console.log('sendToSNS ');

        aws.config.update({region: REGION});
        const sns = new aws.SNS();
        let params = {
            Message: JSON.stringify(message),
            Subject: "Media has been processed by Lambda",
            TopicArn: TOPIC_ARN
        };

        let result = await sns.publish(params).promise().catch((err) => console.error(err, err.stack));

        if (result) {
            console.log(`Message ${params.Message} send sent to the topic ${params.TopicArn}`);
            console.log("MessageID is " + result.MessageId);
        }
    },
    getObjectTagging = function (bucket, key) {
        let options = {
            Bucket: bucket,
            Key: key,
        };

        return s3.getObjectTagging(options).promise();
    },
    getObject = function (bucket, key) {
        let options = {
            Bucket: bucket,
            Key: key,
        };

        return s3.getObject(options).promise();
    },

    getHeadObject = async function (bucket, key) {
        let options = {
            Bucket: bucket,
            Key: key,
        };

        return await s3.headObject(options).promise();
    },

    callbackConvert = function (callback_url, message) {
        console.log("CallbackConvert");

        const data = JSON.stringify(message);

        const match = callback_url.match(/^(https?\:)\/\/(([^:\/?#]*)(?:\:([0-9]+))?)([\/]{0,1}[^?#]*)(\?[^#]*|)(#.*|)$/);

        const options = {
            hostname: match[2],
            port: 443,
            path: match[5],
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json',
                'Content-Length': data.length
            }
        }

        const req = https.request(options, res => {
            console.log(`statusCode: ${res.statusCode}`)

            res.on('data', d => {
                process.stdout.write(d)
            })
        })

        req.on('error', error => {
            console.error(error)
        })

        req.write(data)
        req.end()
    },
    removeFile = function (path) {
        try {
            fs.unlinkSync(path)
            //file removed
        } catch(err) {
            console.error(err)
        }
    }

module.exports = {
    downloadFileFromS3,
    uploadFileToS3,
    sendToSNS,
    getObjectTagging,
    getObject,
    getHeadObject,
    callbackConvert,
    removeFile
};